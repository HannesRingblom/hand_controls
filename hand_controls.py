import cv2
import mediapipe as mp
import time
import socket
HOST = "172.20.10.2"
PORT = 12000
cap = cv2.VideoCapture(0)
mpHands = mp.solutions.hands
hands = mpHands.Hands()
mpDraw = mp.solutions.drawing_utils
fingers_right = []
fingers_left = []
hand_count = 0
command_count = 0
command_to_call = ''
last_command = ''
start_time = 0
control_time = 0
pTime, cTime = 0, 9
counta = 0
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    print("Trying to connect")
    s.connect((HOST, PORT))
    while True:
        success, img = cap.read()
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = hands.process(imgRGB)
        # print(results.multi_hand_landmarks)
        if results.multi_hand_landmarks:
            counta += 1
            # print(counta)
            for handLms in results.multi_hand_landmarks:
                for landmark_id, lm in enumerate(handLms.landmark):
                    h, w, c = img.shape
                    cx, cy = int(lm.x * w), int(lm.y * h)
                    if hand_count == 0:
                        if landmark_id == 0 or landmark_id == 4:
                            fingers_left.append(lm)
                            cv2.circle(img, (cx, cy), 10, (255, 0, 255), cv2.FILLED)
                            # print(landmark_id, cx, cy)
                    else:
                        if landmark_id == 0 or landmark_id == 4:
                            fingers_right.append(lm)
                            cv2.circle(img, (cx, cy), 10, (255, 0, 255), cv2.FILLED)

                hand_count = 1
                # mpDraw.draw_landmarks(img, handLms, mpHands.HAND_CONNECTIONS)
            hand_count = 0
        if counta % 10 == 0:
            if fingers_right and fingers_left:
                if fingers_right[1].y < (fingers_right[0].y - 0.1):
                    if fingers_left[1].y < (fingers_left[0].y - 0.1):
                        command_to_call = 'w'
                        command_count += 1
                        print("Both UP")
                    elif fingers_left[1].y > (fingers_left[0].y + 0.1):
                        command_to_call = 'a'
                        command_count += 1
                        print("Right UP   Left DOWN")
                elif fingers_right[1].y > (fingers_right[0].y + 0.1):
                    if fingers_left[1].y > (fingers_left[0].y + 0.1):
                        command_to_call = 's'
                        command_count += 1
                        print("Both DOWN")
                    elif fingers_left[1].y < (fingers_left[0].y - 0.1):
                        command_to_call = 'd'
                        command_count += 1
                        print("Right DOWN   Left UP")
            elif fingers_left:
                if fingers_right[1].y < (fingers_right[0].y - 0.1):
                    command_to_call = 'c'
                    command_count += 1
                    print("ONE THUMB UP")
                elif fingers_right[1].y > (fingers_right[0].y + 0.1):
                    command_to_call = 'x'
                    command_count += 1
                    print("ONE THUMB DOWN")
        if last_command != command_to_call:
            command_count = 1
            last_command = command_to_call
        if command_count == 4:
            # s.sendto(command_to_call.encode())
            print("Sending commmand")
            s.sendto(command_to_call.encode(), (HOST, PORT))
            command_count = 0
        cTime = time.time()
        fps = 1 / (cTime - pTime)
        pTime = cTime
        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3,
                    (255, 0, 255), 3)
        fingers_right = []
        fingers_left = []
        cv2.imshow('Hand Steering', cv2.flip(img, 1))
        if cv2.waitKey(1) == 27:
            break
    if cv2.waitKey(1) == 27:
        break
cap.release()
cv2.destroyAllWindows()
